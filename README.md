# NOTE

云计算实验5 所需要的东西

* `cpu-stat`, `grpc` 服务端, 用于获取主机的 `CPU` 占用率信息
* `mongo_lb_test`, `rust` 写的测试用例, 使用 `grpc` 去定期获取 CPU 信息, 过滤出CPU占用率低于阈值的节点, 然后随机挑选一个节点发送请求.

## RUN Mongo

```shell
docker-compose up --build
```

## RUN test

```shell
cd mongo_lb_test
RUST_LOG=info cargo r -- "node-1|http://<node-1>:28017/test/1234|<node-1>:4000" "node-2|http://<node-2>:28017/test/1234|<node-2>:4000"
```