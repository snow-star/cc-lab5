import rpc_pb2
import rpc_pb2_grpc
import psutil
import grpc
from concurrent import futures
import time
import sys


class StatusService(rpc_pb2_grpc.StatusServiceServicer):
    def GetStatus(self, request: rpc_pb2.EmptyRequest, context):
        cpu = psutil.cpu_percent()
        name = sys.argv[1]
        
        return rpc_pb2.StatReply(name = name, cpu = cpu)


def run():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    rpc_pb2_grpc.add_StatusServiceServicer_to_server(StatusService(), server)

    server.add_insecure_port("0.0.0.0:4000")
    server.start()

    try:
        while True:
            time.sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == "__main__":
    run()
