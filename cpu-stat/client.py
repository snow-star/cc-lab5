import grpc
import rpc_pb2_grpc
import rpc_pb2
import sys


def run():
    channel = grpc.insecure_channel(sys.argv[1])
    stub = rpc_pb2_grpc.StatusServiceStub(channel)
    resp = stub.GetStatus(rpc_pb2.EmptyRequest())
    print(resp)

if __name__ == "__main__":
    run()
