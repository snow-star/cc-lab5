fn main() {
    let root = "src/protos";

    println!("cargo:rerun-if-changed={}", root);

    protoc_grpcio::compile_grpc_protos(&["rpc.proto"], &[root], &root, None)
        .expect("Failed compile");
}
