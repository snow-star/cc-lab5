// This file is generated. Do not edit
// @generated

// https://github.com/Manishearth/rust-clippy/issues/702
#![allow(unknown_lints)]
#![allow(clippy)]

#![cfg_attr(rustfmt, rustfmt_skip)]

#![allow(box_pointers)]
#![allow(dead_code)]
#![allow(missing_docs)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(trivial_casts)]
#![allow(unsafe_code)]
#![allow(unused_imports)]
#![allow(unused_results)]

const METHOD_STATUS_SERVICE_GET_STATUS: ::grpcio::Method<super::rpc::EmptyRequest, super::rpc::StatReply> = ::grpcio::Method {
    ty: ::grpcio::MethodType::Unary,
    name: "/StatusService/GetStatus",
    req_mar: ::grpcio::Marshaller { ser: ::grpcio::pb_ser, de: ::grpcio::pb_de },
    resp_mar: ::grpcio::Marshaller { ser: ::grpcio::pb_ser, de: ::grpcio::pb_de },
};

#[derive(Clone)]
pub struct StatusServiceClient {
    client: ::grpcio::Client,
}

impl StatusServiceClient {
    pub fn new(channel: ::grpcio::Channel) -> Self {
        StatusServiceClient {
            client: ::grpcio::Client::new(channel),
        }
    }

    pub fn get_status_opt(&self, req: &super::rpc::EmptyRequest, opt: ::grpcio::CallOption) -> ::grpcio::Result<super::rpc::StatReply> {
        self.client.unary_call(&METHOD_STATUS_SERVICE_GET_STATUS, req, opt)
    }

    pub fn get_status(&self, req: &super::rpc::EmptyRequest) -> ::grpcio::Result<super::rpc::StatReply> {
        self.get_status_opt(req, ::grpcio::CallOption::default())
    }

    pub fn get_status_async_opt(&self, req: &super::rpc::EmptyRequest, opt: ::grpcio::CallOption) -> ::grpcio::Result<::grpcio::ClientUnaryReceiver<super::rpc::StatReply>> {
        self.client.unary_call_async(&METHOD_STATUS_SERVICE_GET_STATUS, req, opt)
    }

    pub fn get_status_async(&self, req: &super::rpc::EmptyRequest) -> ::grpcio::Result<::grpcio::ClientUnaryReceiver<super::rpc::StatReply>> {
        self.get_status_async_opt(req, ::grpcio::CallOption::default())
    }
    pub fn spawn<F>(&self, f: F) where F: ::futures::Future<Item = (), Error = ()> + Send + 'static {
        self.client.spawn(f)
    }
}

pub trait StatusService {
    fn get_status(&mut self, ctx: ::grpcio::RpcContext, req: super::rpc::EmptyRequest, sink: ::grpcio::UnarySink<super::rpc::StatReply>);
}

pub fn create_status_service<S: StatusService + Send + Clone + 'static>(s: S) -> ::grpcio::Service {
    let mut builder = ::grpcio::ServiceBuilder::new();
    let mut instance = s.clone();
    builder = builder.add_unary_handler(&METHOD_STATUS_SERVICE_GET_STATUS, move |ctx, req, resp| {
        instance.get_status(ctx, req, resp)
    });
    builder.build()
}
