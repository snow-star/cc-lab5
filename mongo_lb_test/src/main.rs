use tokio::runtime::{Builder, Runtime};

use std::sync::Arc;
use std::sync::RwLock;

use futures::prelude::*;
use grpcio::{ChannelBuilder, EnvBuilder};
use log::{error, info};
use reqwest::r#async::Client;
use std::thread::sleep;
use std::time::Duration;

use protos::{rpc::EmptyRequest, rpc::StatReply, rpc_grpc::StatusServiceClient};

use clap::{App as ClapApp, Arg};
use rand::seq::IteratorRandom;
use std::error::Error;

mod protos;

#[derive(Clone, Debug)]
struct MongoNode {
    name: String,
    cpu: f64,

    mongo_endpoint: String,
    rpc_endpoint: String,
}

struct App {
    nodes: Arc<RwLock<Vec<MongoNode>>>,
    rt: Runtime,
    threshold: f64,
    interval: u64,
}

impl App {
    pub fn new(nodes: Vec<MongoNode>, threshold: f64, interval: u64) -> Self {
        App {
            nodes: Arc::new(RwLock::new(nodes)),
            rt: Builder::new()
                .core_threads(16)
                .blocking_threads(400)
                .build()
                .unwrap(),
            threshold,
            interval,
        }
    }

    pub fn run(&mut self) {
        self.update_cpu_stats();

        loop {
            let client = Client::new();
            let node = self.pick();

            if let Some(node) = node {
                info!(
                    "requesting mongodb at {name}({address}), cpu stat: {cpu:.2}%",
                    name = node.name,
                    address = node.mongo_endpoint,
                    cpu = node.cpu
                );

                let resp = client
                    .get(&node.mongo_endpoint)
                    .send()
                    .map(|_| ())
                    .map_err(|e| error!("{}", e.description()));

                self.rt.spawn(resp);
            }

            sleep(Duration::from_millis(self.interval));
        }
    }

    pub fn pick(&self) -> Option<MongoNode> {
        let nodes = self.nodes.read().unwrap();

        nodes
            .iter()
            .filter(|&node| node.cpu < self.threshold)
            .choose(&mut rand::thread_rng())
            .map(|node| node.clone())
    }

    pub fn update_cpu_stats(&self) {
        let nodes = Arc::clone(&self.nodes);
        std::thread::spawn(move || loop {
            {
                let mut nodes = nodes.write().unwrap();
                for node in &mut *nodes {
                    let env = Arc::new(EnvBuilder::new().build());
                    let ch = ChannelBuilder::new(env).connect(&node.rpc_endpoint);
                    let client = StatusServiceClient::new(ch);

                    let req = EmptyRequest::new();
                    let resp: grpcio::Result<StatReply> = client.get_status(&req);
                    if let Ok(resp) = resp {
                        node.cpu = resp.cpu as f64
                    }
                }
            }

            sleep(Duration::new(1, 0));
        });
    }
}

fn main() {
    env_logger::init();

    let matches = ClapApp::new("mongo lb test")
        .arg(
            Arg::with_name("nodes")
                .value_name("nodes")
                .required(true)
                .multiple(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("threshold")
                .long("threshold")
                .short("t")
                .value_name("threshold")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("interval")
                .long("interval")
                .short("i")
                .value_name("interval")
                .takes_value(true),
        )
        .get_matches();

    let nodes = matches.values_of("nodes").unwrap();

    let nodes: Vec<MongoNode> = nodes
        .map(|it| {
            let frags: Vec<&str> = it.split("|").collect();

            let name = frags[0];
            let mongo_endpoint = frags[1];
            let rpc_endpoint = frags[2];

            MongoNode {
                name: name.to_owned(),
                cpu: 0f64,
                mongo_endpoint: mongo_endpoint.to_owned(),
                rpc_endpoint: rpc_endpoint.to_owned(),
            }
        })
        .collect();

    let threshold = matches
        .value_of("threshold")
        .and_then(|it| it.parse::<f64>().ok())
        .unwrap_or(75f64);

    let interval = matches
        .value_of("interval")
        .and_then(|it| it.parse::<u64>().ok())
        .unwrap_or(10);

    let mut app = App::new(nodes, threshold, interval);
    app.run();
}
